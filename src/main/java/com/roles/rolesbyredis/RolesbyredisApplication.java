package com.roles.rolesbyredis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RolesbyredisApplication {

	public static void main(String[] args) {
		SpringApplication.run(RolesbyredisApplication.class, args);
	}

}
